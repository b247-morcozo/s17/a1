/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-show an alert to thank the user for their input.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

			function printUserInfo(){
				let myName = prompt("What is your name?");
				let myAge = prompt("How old are you?");
				let myAddress = prompt("Where do you live?");
				alert("Thank you for your input! ");
				console.log("Hello, " + myName);
				console.log("You are " + myAge + " years old.");
				console.log("You live in " + myAddress);
			}

			printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

			function printMyTopFiveBands(){
				console.log("1. Parokya Ni Edgar");
				console.log("2. Blink 182");
				console.log("3. Linkin Park");
				console.log("4. Kamikazee");
				console.log("5. Plain White T's");
			}

			printMyTopFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

			function printMyTopFiveMovies(){
				console.log("1. The Godfather");
				console.log("Rotten Tomatoes Rating: 97%")
				console.log("2. The Green Mile ");
				console.log("Rotten Tomatoes Rating: 79%")
				console.log("3. Iron Man");
				console.log("Rotten Tomatoes Rating: 94%")
				console.log("4. Avengers: Endgame");
				console.log("Rotten Tomatoes Rating: 94%")
				console.log("5. Avengers: Infinity War");
				console.log("Rotten Tomatoes Rating: 85%")
			}

			printMyTopFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

			let printFriends = function printUsers(){
				alert("Hi! Please add the names of your friends.");
				let friend1 = prompt("Enter your first friend's name:"); 
				let friend2 = prompt("Enter your second friend's name:"); 
				let friend3 = prompt("Enter your third friend's name:");

				console.log("You are friends with:")
				console.log(friend1); 
				console.log(friend2); 
				console.log(friend3); 
			};
			printFriends();

			//console.log(friend1);
			//console.log(friend2);
